-----------------
-----------------


   ![Image000](Images/PANDORA.png){width=5%} |
**Data Analysis of PANDORA**  |
Developed within the framework of IEA ECBC Annex86  
Last update: December, 2024

Within the framework of <a href="https://annex86.iea-ebc.org/">IEA ECBC Annex86</a>, an analysis has been conducted to extract Emission Rates (ER) usually required by IAQ modelers from <a href="https://db-pandora.univ-lr.fr/">PANDORA</a>.

The analysis focused on four target pollutants: PM2.5, formaldehyde, benzene and TVOC. For each pollutant, the data were compiled based on their primary indoor sources, namely Construction and Decoration Materials, Furniture and Occupants and Occupants' Activities (including the use of cleaning products). While most of the results presented on the webpage are straightforward compilations, the Construction and Decoration Materials category offers a more detailed analysis due to the large volume of data available in the database. Note that the results are displayed in figures, and the associated statistics (mean, standard deviation, min., P25, median, P75 and max.) can be downloaded for external use.


**Table of Contents**

[[_TOC_]]

-----------------
-----------------

# PM2.5 

## Occupants and Occupant Activities 
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number. Some data are raw data from the PANDORA database, while others are the result of an integral calculation when the original data were spectral (emission rate according to particle diameter).  

![Image001](PM25/Sources/01_PM25_CookingActivity.png){width=25%}
![Image002](PM25/Sources/02_PM25_UsingAerosol.png){width=25%}
![Image003](PM25/Sources/03_PM25_CleaningActivity.png){width=25%}
![Image004](PM25/Sources/04_PM25_CookingAppliances.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[PM2.5 Emission rates](PM25/Sources/PM25_Sources.txt)
</details>  



# Formaldehyde

## Construction and Decoration Materials 
### Data 
<details>
<summary> Graphs - All years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.  
Numbers of data are specified in each graph as "#".  

![Image001](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Acoustical Materials_all.png){width=25%}
![Image002](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Carpeting_all.png){width=25%}
![Image003](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Finishes_all.png){width=25%}
![Image004](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Flooring_all.png){width=25%}
![Image005](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Furnishing Materials_all.png){width=25%}
![Image006](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Installation Materials_all.png){width=25%}
![Image007](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Insulation Materials_all.png){width=25%}
![Image008](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Interior Panels_all.png){width=25%}
![Image009](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Structural Materials_all.png){width=25%}
![Image010](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Wall Covering_all.png){width=25%}  
</details>
<details>
<summary> Graphs - By years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.
Numbers of data are specified in each graph as "#".  
 
![Image011](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Acoustical Materials_byYears.png){width=25%}
![Image012](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Carpeting_byYears.png){width=25%}
![Image013](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Finishes_byYears.png){width=25%}
![Image014](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Flooring_byYears.png){width=25%}
![Image015](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Furnishing Materials_byYears.png){width=25%}
![Image016](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Installation Materials_byYears.png){width=25%}
![Image017](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Insulation Materials_byYears.png){width=25%}
![Image018](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Interior Panels_byYears.png){width=25%}
![Image019](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Structural Materials_byYears.png){width=25%}
![Image020](Formaldehyde/Construction and Decoration Materials/Formaldehyde-Wall Covering_byYears.png){width=25%}  
</details>

<details>
<summary> Download data </summary>
[Formaldehyde Emission rates per types for all materials](Formaldehyde/Construction and Decoration Materials/Formaldehyde.txt)
</details>  

### Meta-data
<details>
<summary> Definition </summary>
In the previous results, sources were still separated by sub-categories i.e. Carpeting, Acoustical Materials, Finishes, Flooring, Furnishing Materials, Installation Materials, Interior Panels, Structural Materials, Insulation Materials, Wall Covering and Openings. In that way, IAQ modelers must select the emission rates corresponding to the materials composing the floor, ceiling and vertical walls of the indoor environment they need to model. To facilitate the process of calculating a global emission rate that accounts for all those building parts, we calculate an ultimate aggregated data called **meta-data** that is related to the geometrical surface area i.e. the sum of the floor, ceiling and vertical walls surface areas. However, in its current state, there is no dedicated field in the database to distinguish the use of a particular material for the floor, vertical walls and ceiling. As a result, all materials are applied to each one of the surfaces with the same probability. In this way, there is no dependency on the building/room dimensions. Therefore, the meta-data statistics are calculated based on one statistical value (mean or maximal) of each material category (Acoustical Materials, Carpeting...) previously calculated and the number of available data. The methodology employed here is based on the rules of the French LCA database INIES (INIES, 2022) to calculate default values when specific values are not available:  

- If only one value is available, then the resulting value is multiplied by two;  
- If two values are available, then the resulting value is the maximal value of the two available values multiplied by 1.3; 
- If there are more than two available values, then the resulting value is the mean value multiplied by 1.3.  
 
 
When available, the **French Label color-scale** for construction and decoration materials VOC emission (French Decree April 19th, 2011) is displayed for the emission at 28 days. The color code used is green (A+ = low emission), light green (A), yellow (B) and red (C = high emission). These calculations considered that the material is applied to all internal surfaces (ALL), only the Floor or Ceiling (F/C) and only vertical walls (VERT). The considered room for this calculation has the following dimensions and air change rate: 4.0 × 3.0 × 2.5 m³ and 0.5 vol/h of clean air.   

_French Decree April 19th, 2011. Labeling of construction products, wall or floor coverings, paints and varnishes on their volatile pollutant emissions. April 19th. https://www.legifrance.gouv.fr/loda/id/JORFTEXT000023991852._
_INIES. 2022. Procedure for developing default environmental data (DED) relating to construction products and equipment for use in the method for assessing the energy and environmental performance of new buildings. Report (in French), Version 4 – November 2022, 46p._
</details>

<details>
<summary> Graph </summary>
![Formaldehyde_Meta](Formaldehyde/Construction and Decoration Materials/Formaldehyde_Meta.png){width=50%}
</details>

<details>
<summary> Download data </summary>
[Formaldehyde Meta emission rates](Formaldehyde/Construction and Decoration Materials/Formaldehyde_Meta.txt) 
</details>  

## Furniture 
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](Formaldehyde/Furniture/01_Formaldehyde_DayNurseryFurniture.png){width=25%}
![Image002](Formaldehyde/Furniture/02_Formaldehyde_OtherFurn.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[Formaldehyde Emission rates per types for all Furniture](Formaldehyde/Furniture/Formaldehyde_Furniture.txt)
</details>  


## Other Sources (Occupants...)
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](Formaldehyde/Other Sources/01_Formaldehyde_Occupants.png){width=25%}
![Image002](Formaldehyde/Other Sources/02_Formaldehyde_Painting.png){width=25%}
![Image003](Formaldehyde/Other Sources/03_Formaldehyde_Plastic arts.png){width=25%}
![Image004](Formaldehyde/Other Sources/04_Formaldehyde_Candles and incenses.png){width=25%}
![Image005](Formaldehyde/Other Sources/05_Formaldehyde_Cleaning products.png){width=25%}
![Image006](Formaldehyde/Other Sources/06_Formaldehyde_Electrical equipment.png){width=25%}
![Image007](Formaldehyde/Other Sources/07_Formaldehyde_Space Heaters Time.png){width=25%}
![Image008](Formaldehyde/Other Sources/08_Formaldehyde_Space Heaters Fuel Consumption.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[Formaldehyde Emission rates per types for all other Sources](Formaldehyde/Other Sources/Formaldehyde_Other Sources.txt)
</details>  

-----------
-----------


# Benzene


## Construction and Decoration Materials 

### Data 

<details>
<summary> Graphs - All years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.
Numbers of data are specified in each graph as "#".  

![Image001](Benzene/Construction and Decoration Materials/Benzene-Acoustical Materials_all.png){width=25%}
![Image002](Benzene/Construction and Decoration Materials/Benzene-Carpeting_all.png){width=25%}
![Image003](Benzene/Construction and Decoration Materials/Benzene-Finishes_all.png){width=25%}
![Image004](Benzene/Construction and Decoration Materials/Benzene-Flooring_all.png){width=25%}
![Image005](Benzene/Construction and Decoration Materials/Benzene-Furnishing Materials_all.png){width=25%}
![Image006](Benzene/Construction and Decoration Materials/Benzene-Installation Materials_all.png){width=25%}
![Image008](Benzene/Construction and Decoration Materials/Benzene-Interior Panels_all.png){width=25%}
![Image009](Benzene/Construction and Decoration Materials/Benzene-Structural Materials_all.png){width=25%}
![Image010](Benzene/Construction and Decoration Materials/Benzene-Wall Covering_all.png){width=25%}  
</details>
<details>
<summary> Graphs - By years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.
Numbers of data are specified in each graph as "#".  

![Image011](Benzene/Construction and Decoration Materials/Benzene-Acoustical Materials_byYears.png){width=25%}
![Image012](Benzene/Construction and Decoration Materials/Benzene-Carpeting_byYears.png){width=25%}
![Image013](Benzene/Construction and Decoration Materials/Benzene-Finishes_byYears.png){width=25%}
![Image014](Benzene/Construction and Decoration Materials/Benzene-Flooring_byYears.png){width=25%}
![Image015](Benzene/Construction and Decoration Materials/Benzene-Furnishing Materials_byYears.png){width=25%}
![Image016](Benzene/Construction and Decoration Materials/Benzene-Installation Materials_byYears.png){width=25%}
![Image018](Benzene/Construction and Decoration Materials/Benzene-Interior Panels_byYears.png){width=25%}
![Image019](Benzene/Construction and Decoration Materials/Benzene-Structural Materials_byYears.png){width=25%}
![Image020](Benzene/Construction and Decoration Materials/Benzene-Wall Covering_byYears.png){width=25%}   
</details>

<details>
<summary> Download data </summary>
[Benzene Emission rates per types for all materials](Benzene/Construction and Decoration Materials/Benzene.txt)  
</details>  

### Meta-data
<details>
<summary> Definition </summary>
In the previous results, sources were still separated by sub-categories i.e. Carpeting, Acoustical Materials, Finishes, Flooring, Furnishing Materials, Installation Materials, Interior Panels, Structural Materials, Insulation Materials, Wall Covering and Openings. In that way, IAQ modelers must select the emission rates corresponding to the materials composing the floor, ceiling and vertical walls of the indoor environment they need to model. To facilitate the process of calculating a global emission rate that accounts for all those building parts, we calculate an ultimate aggregated data called **meta-data** that is related to the geometrical surface area i.e. the sum of the floor, ceiling and vertical walls surface areas. However, in its current state, there is no dedicated field in the database to distinguish the use of a particular material for the floor, vertical walls and ceiling. As a result, all materials are applied to each one of the surfaces with the same probability. In this way, there is no dependency on the building/room dimensions. Therefore, the meta-data statistics are calculated based on one statistical value (mean or maximal) of each material category (Acoustical Materials, Carpeting...) previously calculated and the number of available data. The methodology employed here is based on the rules of the French LCA database INIES (INIES, 2022) to calculate default values when specific values are not available:  

- If only one value is available, then the resulting value is multiplied by two;  
- If two values are available, then the resulting value is the maximal value of the two available values multiplied by 1.3; 
- If there are more than two available values, then the resulting value is the mean value multiplied by 1.3.  
 
 
When available, the **French Label color-scale** for construction and decoration materials VOC emission (French Decree April 19th, 2011) is displayed for the emission at 28 days. The color code used is green (A+ = low emission), light green (A), yellow (B) and red (C = high emission). These calculations considered that the material is applied to all internal surfaces (ALL), only the Floor or Ceiling (F/C) and only vertical walls (VERT). The considered room for this calculation has the following dimensions and air change rate: 4.0 × 3.0 × 2.5 m³ and 0.5 vol/h of clean air.   

_French Decree April 19th, 2011. Labeling of construction products, wall or floor coverings, paints and varnishes on their volatile pollutant emissions. April 19th. https://www.legifrance.gouv.fr/loda/id/JORFTEXT000023991852._
_INIES. 2022. Procedure for developing default environmental data (DED) relating to construction products and equipment for use in the method for assessing the energy and environmental performance of new buildings. Report (in French), Version 4 – November 2022, 46p._
</details>

<details>
<summary> Graph </summary>
![Benzene_Meta](Benzene/Construction and Decoration Materials/Benzene_Meta.png){width=50%}
</details>

<details>
<summary> Download data </summary>
[Benzene Meta emission rates](Benzene/Construction and Decoration Materials/Benzene_Meta.txt)  
</details>  

## Furniture 
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](Benzene/Furniture/01_Benzene_DayNurseryFurn.png){width=25%}
![Image002](Benzene/Furniture/02_Benzene_OtherFurniture.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[Benzene Emission rates per types for all Furniture](Benzene/Furniture/Benzene_Furniture.txt)
</details>  


## Other Sources (Occupants...)
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](Benzene/Other Sources/01_Benzene_Occupants.png){width=25%}
![Image002](Benzene/Other Sources/02_Benzene_Painting.png){width=25%}
![Image003](Benzene/Other Sources/03_Benzene_PlasticArts.png){width=25%}
![Image004](Benzene/Other Sources/04_Benzene_CandleIncense.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[Benzene Emission rates per types for all other Sources](Benzene/Other Sources/Benzene_Other Sources.txt)
</details>  

-----------
-----------


# TVOC


## Construction and Decoration Materials 

### Data 

<details>
<summary> Graphs - All years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.
Numbers of data are specified in each graph as "#".  

![Image001](TVOC/Construction and Decoration Materials/TVOC-Acoustical Materials_all.png){width=25%}
![Image002](TVOC/Construction and Decoration Materials/TVOC-Carpeting_all.png){width=25%}
![Image003](TVOC/Construction and Decoration Materials/TVOC-Finishes_all.png){width=25%}
![Image004](TVOC/Construction and Decoration Materials/TVOC-Flooring_all.png){width=25%}
![Image005](TVOC/Construction and Decoration Materials/TVOC-Furnishing Materials_all.png){width=25%}
![Image006](TVOC/Construction and Decoration Materials/TVOC-Installation Materials_all.png){width=25%}
![Image007](TVOC/Construction and Decoration Materials/TVOC-Insulation Materials_all.png){width=25%}
![Image008](TVOC/Construction and Decoration Materials/TVOC-Interior Panels_all.png){width=25%}
![Image011](TVOC/Construction and Decoration Materials/TVOC-Openings_all.png){width=25%}  
![Image009](TVOC/Construction and Decoration Materials/TVOC-Structural Materials_all.png){width=25%}
![Image010](TVOC/Construction and Decoration Materials/TVOC-Wall Covering_all.png){width=25%}   
</details>
<details>
<summary> Graphs - By years </summary>
As there is a lot of data in this category, a graphical representation in the form of a whisker box has been chosen.
Numbers of data are specified in each graph as "#".  

![Image011](TVOC/Construction and Decoration Materials/TVOC-Acoustical Materials_byYears.png){width=25%}
![Image012](TVOC/Construction and Decoration Materials/TVOC-Carpeting_byYears.png){width=25%}
![Image013](TVOC/Construction and Decoration Materials/TVOC-Finishes_byYears.png){width=25%}
![Image014](TVOC/Construction and Decoration Materials/TVOC-Flooring_byYears.png){width=25%}
![Image015](TVOC/Construction and Decoration Materials/TVOC-Furnishing Materials_byYears.png){width=25%}
![Image016](TVOC/Construction and Decoration Materials/TVOC-Installation Materials_byYears.png){width=25%}
![Image017](TVOC/Construction and Decoration Materials/TVOC-Insulation Materials_byYears.png){width=25%}
![Image018](TVOC/Construction and Decoration Materials/TVOC-Interior Panels_byYears.png){width=25%}
![Image021](TVOC/Construction and Decoration Materials/TVOC-Openings_byYears.png){width=25%}
![Image019](TVOC/Construction and Decoration Materials/TVOC-Structural Materials_byYears.png){width=25%}
![Image020](TVOC/Construction and Decoration Materials/TVOC-Wall Covering_byYears.png){width=25%} 
</details>

<details>
<summary> Download data </summary>
[TVOC Emission rates per types for all materials](TVOC/Construction and Decoration Materials/TVOC.txt)  

</details>  

### Meta-data
<details>
<summary> Definition </summary>
In the previous results, sources were still separated by sub-categories i.e. Carpeting, Acoustical Materials, Finishes, Flooring, Furnishing Materials, Installation Materials, Interior Panels, Structural Materials, Insulation Materials, Wall Covering and Openings. In that way, IAQ modelers must select the emission rates corresponding to the materials composing the floor, ceiling and vertical walls of the indoor environment they need to model. To facilitate the process of calculating a global emission rate that accounts for all those building parts, we calculate an ultimate aggregated data called **meta-data** that is related to the geometrical surface area i.e. the sum of the floor, ceiling and vertical walls surface areas. However, in its current state, there is no dedicated field in the database to distinguish the use of a particular material for the floor, vertical walls and ceiling. As a result, all materials are applied to each one of the surfaces with the same probability. In this way, there is no dependency on the building/room dimensions. Therefore, the meta-data statistics are calculated based on one statistical value (mean or maximal) of each material category (Acoustical Materials, Carpeting...) previously calculated and the number of available data. The methodology employed here is based on the rules of the French LCA database INIES (INIES, 2022) to calculate default values when specific values are not available:  

- If only one value is available, then the resulting value is multiplied by two;  
- If two values are available, then the resulting value is the maximal value of the two available values multiplied by 1.3; 
- If there are more than two available values, then the resulting value is the mean value multiplied by 1.3.  
 
 
When available, the **French Label color-scale** for construction and decoration materials VOC emission (French Decree April 19th, 2011) is displayed for the emission at 28 days. The color code used is green (A+ = low emission), light green (A), yellow (B) and red (C = high emission). These calculations considered that the material is applied to all internal surfaces (ALL), only the Floor or Ceiling (F/C) and only vertical walls (VERT). The considered room for this calculation has the following dimensions and air change rate: 4.0 × 3.0 × 2.5 m³ and 0.5 vol/h of clean air.   

_French Decree April 19th, 2011. Labeling of construction products, wall or floor coverings, paints and varnishes on their volatile pollutant emissions. April 19th. https://www.legifrance.gouv.fr/loda/id/JORFTEXT000023991852._
_INIES. 2022. Procedure for developing default environmental data (DED) relating to construction products and equipment for use in the method for assessing the energy and environmental performance of new buildings. Report (in French), Version 4 – November 2022, 46p._
</details>

<details>
<summary> Graph </summary>
![TVOC_Meta](TVOC/Construction and Decoration Materials/TVOC_Meta.png){width=50%}
</details>

<details>
<summary> Download data </summary>
[TVOC Meta emission rates](TVOC/Construction and Decoration Materials/TVOC_Meta.txt)  
</details>  

## Furniture 
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](TVOC/Furniture/01_TVOC_Furniture.png){width=25%}
</details>

<details>
<summary> Download data </summary>
[TVOC Emission rates per types for all Furniture](TVOC/Furniture/TVOC_Furniture.txt)
</details>  


## Other Sources (Occupants...)
<details>
<summary> Graphs </summary>
All available data are presented here as they are few in number.  

![Image001](TVOC/Other Sources/03_TVOC_CleaningProducts.png){width=25%}
![Image002](TVOC/Other Sources/02_TVOC_PrinterPhotocopier.png){width=25%}
![Image003](TVOC/Other Sources/01_TVOC_Computer.png){width=25%}

</details>

<details>
<summary> Download data </summary>
[Benzene Emission rates per types for all other Sources](TVOC/Other Sources/TVOC_Other Sources.txt)
</details>  

-----------
-----------




-----------
-----------

  
  
