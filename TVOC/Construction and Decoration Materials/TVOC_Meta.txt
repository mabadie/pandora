Pollutant	Source	Unit	3 days	28 days	365 days
TVOC	Carpeting	Emission rates (µg/(h.m²))	1209.7891489387566	253.51258308462394	75.43282038472668
TVOC	Acoustical Materials	Emission rates (µg/(h.m²))	204.45647394620545	91.8507064084473	28.226246637197338
TVOC	Finishes	Emission rates (µg/(h.m²))	35272.03350773126	2105.9006643498565	176.93420981707231
TVOC	Flooring	Emission rates (µg/(h.m²))	4194.918125740129	338.90996333558695	27.069082944805075
TVOC	Furnishing Materials	Emission rates (µg/(h.m²))	2406.7695676448257	586.2387754895746	44.28702904098078
TVOC	Installation Materials	Emission rates (µg/(h.m²))	1079904.313733019	94455.51578779709	29134.33873793992
TVOC	Interior Panels	Emission rates (µg/(h.m²))	1122.7154516527967	358.2168815890731	42.43695077980842
TVOC	Structural Materials	Emission rates (µg/(h.m²))	650.981514079027	380.1665676391237	115.87136180213255
TVOC	Insulation Materials	Emission rates (µg/(h.m²))	44.40428571428572	26.476666666666667	nan
TVOC	Wall Covering	Emission rates (µg/(h.m²))	588.0333333333333	598.0	nan
TVOC	Openings	Emission rates (µg/(h.m²))	3192.8	2459.6	nan
